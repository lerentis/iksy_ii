# README #

The Requirements for this setup are:

* Docker
* Git

For further information please have a look at our [wiki](https://bitbucket.org/lerentis/iksy_ii/wiki).

### tl;dr ###

If you don't want to read the wiki, please change at least your mysql root password in the corresponding build and run script. 

## What is this repository for? ##

Just a quick Tutorial Page of "how to learn JS" and a Forum to enable communication between users

## Who do I talk to? ##

If you have any issus with this project, post them here or send me a Message